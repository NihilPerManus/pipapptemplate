﻿using MahApps.Metro.IconPacks;
using PipApp5.MVVM.Model.Flyout;
using PipApp5.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TestWPFApp.MVVM.Model.Flyouts
{
    //Default implementation
    public class MaterialFlyout : ObservableClass, IMaterialFlyout

    {
        private Page _View;
        private bool _IsChecked;
        private PackIconMaterialKind _Kind;

        public Page View { get { return _View; } set { _View = value; NotifyPropertyChanged(); } }

        public bool IsChecked { get { return _IsChecked; } set { _IsChecked = value; NotifyPropertyChanged(); } }
        public PackIconMaterialKind Kind { get { return _Kind; } set { _Kind = value; NotifyPropertyChanged(); } }
    }

    public interface IMaterialFlyout : IFlyoutItem<PackIconMaterialKind>
    {
    }
}
