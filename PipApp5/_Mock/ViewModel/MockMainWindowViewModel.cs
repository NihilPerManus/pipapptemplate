﻿using PipApp5._Mock.View;
using PipApp5.MVVM.ViewModel;
using PipApp5.MVVM.ViewModel.MainWindow;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TestWPFApp.MVVM.Model.Flyouts;

namespace PipApp5._Mock.ViewModel
{
    public class MockMainWindowViewModel : ObservableClass, IMainWindowViewModel
    {
        private ObservableCollection<IMaterialFlyout> _Flyouts = new ObservableCollection<IMaterialFlyout>(new List<IMaterialFlyout>() { });

        public Page CurrentView { get; set; }
        public Page CurrentNav { get; set; }
        public ObservableCollection<IMaterialFlyout> Flyouts { get => _Flyouts; set { _Flyouts = value; NotifyPropertyChanged(); } }

        
    }
}
