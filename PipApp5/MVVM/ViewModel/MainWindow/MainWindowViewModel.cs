﻿using MahApps.Metro.IconPacks;
using PipApp5._Mock;
using PipApp5.MVVM.Model.Flyout;
using PipApp5.MVVM.ViewModel.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TestWPFApp.MVVM.Model.Flyouts;

namespace PipApp5.MVVM.ViewModel.MainWindow
{
    
    public class MainWindowViewModel : ObservableClass, IMainWindowViewModel
    {
        private Page _CurrentView;
        private Page _CurrentNav;
        private ObservableCollection<IMaterialFlyout> _Flyouts = new ObservableCollection<IMaterialFlyout>();

        public Page CurrentView { get { return _CurrentView; } set { _CurrentView = value; Debug.WriteLine($"Returned {_CurrentView.Title}"); NotifyPropertyChanged(); } }
        public Page CurrentNav { get { return _CurrentNav; } set { _CurrentNav = value; NotifyPropertyChanged(); } }

        public ObservableCollection<IMaterialFlyout> Flyouts { get => _Flyouts; set { _Flyouts = value; NotifyPropertyChanged(); }  }
    }

   

    public interface IMainWindowViewModel
    {
        Page CurrentView { get; set; }
        Page CurrentNav { get; set; }
        ObservableCollection<IMaterialFlyout> Flyouts { get; set; }
    }
}
