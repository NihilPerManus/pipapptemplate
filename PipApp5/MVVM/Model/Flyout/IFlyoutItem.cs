﻿using System.Windows.Controls;

namespace PipApp5.MVVM.Model.Flyout
{
    public interface IFlyoutItem<TIconType>
    {
        Page View { get; set; }
        bool IsChecked { get; set; }
        TIconType Kind { get; set; }
    }
}