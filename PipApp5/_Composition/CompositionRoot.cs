﻿using PipApp5.MVVM.ViewModel.MainWindow;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PipApp5._Composition
{

    //Derive your entrypoint from this class.
    public abstract class CompositionRoot
    {
        protected Container _container;
        readonly App app = new App() { ShutdownMode = ShutdownMode.OnMainWindowClose };

        // Call this to show the window
        public void Execute<TView, TDataContext>() 
            where TView : Page 
            where TDataContext : class
        {
            Bootstrap();
            CustomInjections();
            _container.Verify();

            Debug.WriteLine($"ID: {GetHashCode()}");
            
            //app.InitializeComponent();

            //initialise the page and the datacontext
            var page = _container.GetInstance<TView>();
            var pageContext = _container.GetInstance<TDataContext>();
            page.DataContext = pageContext;

            //Set the main window
            var window = _container.GetInstance<MainWindow>();
            var windowContext = _container.GetInstance<IMainWindowViewModel>();
            window.DataContext = windowContext;

            //Set the ViewModel data
            windowContext.CurrentView = page;

            //Show the page
            //app.MainWindow = window;

            app.Run(window);
            
        }

        //Place all your injections into _container in this method here
        public abstract void CustomInjections();

        private void Bootstrap()
        {
            _container = new Container();

            _container.RegisterSingleton<MainWindow>();
            _container.RegisterSingleton<IMainWindowViewModel, MainWindowViewModel>();
            
        }
    }
}
